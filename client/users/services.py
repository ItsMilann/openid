import requests
from django.conf import settings
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.exceptions import AuthenticationFailed
from users.serializers import UserSerializer
from users.models import User


def get_or_create_user(data, **kwargs) -> User:
    email = data["email"]
    qs = User.objects.filter(email=email)
    if qs.exists():
        return qs.latest("id")
    serializer = UserSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    return serializer.save(**kwargs)


def exchange_auth_token(token) -> dict:
    """Exchange auth token with user information in auth token."""
    BASE_URL = settings.AUTH_SERVER_BASE_URL
    ID = settings.AUTH_APP_ID
    SECRET = settings.AUTH_APP_SECRET
    URI = f"api/v1/o/open-id/?id={ID}&token={token}&secret={SECRET}"
    URL = BASE_URL + URI
    respose = requests.get(URL)
    if respose.status_code in range(200, 300):
        user = get_or_create_user(respose.json())
        refresh = RefreshToken.for_user(user)
        serializer =UserSerializer(user)
        data = {"refresh": str(refresh), "access": str(refresh.access_token), **serializer.data}
        return data
    raise AuthenticationFailed
