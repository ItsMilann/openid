from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response
from users.services import exchange_auth_token


@api_view(["GET"])
@authentication_classes([])
def login_with_auth_server(request):
    token = request.query_params.get("token")
    data = exchange_auth_token(token)
    return Response({"message": "OK", "status": 200, **data})
