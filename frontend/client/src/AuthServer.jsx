import { React, useEffect, useState } from 'react';
import { json, useNavigate, useLocation } from 'react-router-dom'

function Login(props) {
  let location = useLocation()
  const [user, setUser] = useState()
  const params = new URLSearchParams(location.search)
  const applicationToken = params.get("token")

  useEffect(() => {
    let URL = `http://localhost:8080/api/v1/oauth/token/?token=${applicationToken}`
    fetch(URL)
      .then(res => res.json())
      .then(data => {
        localStorage.setItem("access", data.access);
        setUser(data)
      }).catch(err => console.log(err))

  }, [user]);

  return (
    user?.email ?
      (<div>
        <li>Email: {user.email}</li>
        <li>Name: {user.first_name} {user.last_name}</li>
      </div>) : <div>Unauthenticated</div>
  )

}

export default Login;