import { React } from 'react';

function Login(props) {
  const APP_ID = "1545139795587621"
  const loginWithAuthServer = () => window.open(`http://localhost:3000/consent/?id=${APP_ID}`)
  
  return (
    <button onClick={loginWithAuthServer}> Login in with Auth Server</button>
  );
}

export default Login;