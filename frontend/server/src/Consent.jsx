import React from 'react';
import { useSearchParams, useNavigate } from 'react-router-dom';

function Consent(props) {
  const [searchParams, _] = useSearchParams();
  const navigate = useNavigate()
  const applicationId = "1545139795587621" //searchParams.get("id")
  const URL = `http://localhost:8000/api/v1/o/consent/?id=${applicationId}`
  const accessToken = localStorage.getItem("access")
  
  const acceptConsent = () => {
    let headers = {
      "content-type": "application/json",
      "authorization": `Bearer ${accessToken}`
    }
    let options = { headers: headers }
    accessToken?
    fetch(URL, options)
      .then(res => res.json())
      .then(data => {
        data.token ? window.open(`http://localhost:8888/o/login/?token=${data.token}`) : console.log(data)
      })
      .catch(err => console.log(err))
      : navigate("/login/?next=/consent/")
  }

  return (
    <div>
      Allow this app to access you profile information?
      <button onClick={acceptConsent}>Allow</button>
    </div>
  );
}

export default Consent;