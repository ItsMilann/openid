import { React, useEffect, useState } from 'react';
import { json, useNavigate, useLocation } from 'react-router-dom'

function Login(props) {
  let navigate = useNavigate()
  const location = useLocation()
  let params = new URLSearchParams(location.search)
  const next = params.get("next")
  const accessToken = localStorage.getItem("access")
  const [token, setToken] = useState(accessToken)

  const login = () => {
    const body = { email: "admin@gmail.com", password: "password" }
    let URL = "http://localhost:8000/api/v1/token/"
    let options = {
      method: "post",
      body: JSON.stringify(body),
      headers: {
        "content-type": "application/json",
      }
    }

    fetch(URL, options)
      .then(res => res.json())
      .then(data => {
        console.log(data, "datapnse json")
        localStorage.setItem("access", data.access);
        setToken(data.access)
      }).catch(err => console.log(err))
  }

  useEffect(() => {
    token && next ? navigate(next) : token ? navigate("/") : console.log("Not logged in.")
  }, [token])

  const handleSubmit = () => login()

  return (
    <div>
      <button type='button' onClick={handleSubmit}> Login </button>
    </div>
  );
}

export default Login;