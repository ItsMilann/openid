# Ideabreed OAuth and OpenIDConnect
[Read More . . .](https://openid.net/specs/openid-connect-basic-1_0.html)

## Register your application
---
**POST** [`/o/application/register`]()

**BODY** 
```json
{"name": <application name>, "domain": <domain name>}
```
**RESPONSE**
```json
{"application_id": <app_id>, "application_secret": <secret>}
```
## Ask for user consent & Authorization Code
---
**GET** [/o/consent/?id=<application_id>&scope=user-info]()

**RESPONSE**
```json 
{"authorization_code": <auth code>}
```

## Get User Info
---
**GET** [`o/token/?id=<app_id>&token=<authorization_code>&secret=<application_secret>`]()

**RESPONSE**
```json
{"access": <jwt access token>, "refresh": <refresh token>}
```

## Get Access Token
---
**GET** [`o/token/?id=<app_id>&token=<authorization_code>&secret=<application_secret>`]()

**RESPONSE**
```json
{"access": <jwt access token>, "refresh": <refresh token>}
```

## Get Resource
---
**HEADER** `{"Authorization": "Bearer <access token>"}`

**GET** [`/o/user/info`]()

**RESPONSE**
```json
{
    "username": "<username>",
    "email": "<email>",
    "phone": "<phone>"
    ...
}
```

## Get Resource Alternative
---
Exchange user's information with autorization code without using jwt_access token.

**GET** [`o/open-id/?id=<app_id>&token=<authorization_code>&secret=<application_secret>`]()

**RESPONSE**
```json
{
    "username": "<username>",
    "email": "<email>",
    "phone": "<phone>"
    ...
}
```