from typing import Union

_TypeID = Union[None, str, int]
