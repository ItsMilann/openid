import uuid
from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class TimeStampModel(models.Model):
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Application(TimeStampModel):
    app_id = models.CharField(max_length=16, unique=True, blank=True, primary_key=True)
    name = models.CharField(max_length=255)
    app_secret = models.CharField(max_length=512, unique=True, blank=True)
    domain = models.CharField(max_length=257)

    def save(self, *args, **kwargs):
        self.app_secret = uuid.uuid4().hex.lower()
        self.app_id = uuid.uuid4().int.__str__()[:16]
        return super().save(*args, **kwargs)


class Consent(TimeStampModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    scope = models.JSONField(default=dict)
    blocked = models.BooleanField(default=False)
    token = models.CharField(max_length=256, blank=True)

    def save(self, *args, **kwargs):
        self.token = uuid.uuid4().hex.upper()[:16]
        return super().save(*args, **kwargs)
