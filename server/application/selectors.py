"""Do not import this (selectors) in models module."""
from rest_framework.exceptions import APIException
from application.models import Consent
from application.constants import _TypeID


def has_user_consent(application: _TypeID, user: int) -> bool:
    qs = Consent.objects.filter(application_id=application, user_id=user, blocked=False)
    return qs.exists()


def get_user_from_token(token: str):
    try:
        consent = Consent.objects.get(token=token)
        return consent.user
    except Consent.DoesNotExist as e:
        raise APIException("Consent not found!") from e
    except Consent.MultipleObjectsReturned as e:
        raise APIException("Consent not found!") from e
