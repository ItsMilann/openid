from rest_framework import serializers
from application import models


class ConsentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Consent
        fields = "__all__"


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Application
        fields = "__all__"
