"""Do not import this (services) in models module."""
from application.models import Consent
from application.constants import _TypeID


def create_user_consent(user_id: int, application_id: _TypeID):
    instance, _ = Consent.objects.get_or_create(application_id=application_id, user_id=user_id)
    return instance
