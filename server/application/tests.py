from django.test import TestCase
from django.contrib.auth import get_user_model
from application.models import Application, Consent
from application import selectors, services

User = get_user_model()


class TestApplication(TestCase):
    def setUp(self) -> None:
        Application.objects.create(name="Application One", domain="appone.com")
        User.objects.create(username="user_i")

    def test_has_app_id(self):
        app = Application.objects.get(domain="appone.com")
        self.assertTrue(app.app_id is not None)
        self.assertTrue(app.app_secret is not None)

    def test_user_consent(self):
        app = Application.objects.get(domain="appone.com")
        user = User.objects.get(username="user_i")
        consent = selectors.has_user_consent(app.app_id, user.pk)
        self.assertFalse(consent)

    def test_provide_consent(self):
        app = Application.objects.get(domain="appone.com")
        user = User.objects.get(username="user_i")
        consent = services.create_user_consent(user.pk, app.app_id)
        self.assertIsInstance(consent, Consent)
