from rest_framework import exceptions
from application import models


class ValidToken:
    def __init__(self, app_id: str, app_secret: str, token: str) -> None:
        self.app_id = app_id
        self.app_secret = app_secret
        self.token = token

    def is_secret_valid(self):
        try:
            app = models.Application.objects.get(app_id=self.app_id)
            if not app.app_secret == self.app_secret:
                raise exceptions.NotAcceptable("Invalid app secret.")
        except models.Application.DoesNotExist as e:
            raise exceptions.NotFound("Application does not exist") from e
        except models.Application.MultipleObjectsReturned as e:
            raise exceptions.NotFound("Multiple apps exist") from e

    def is_token_valid_for_app(self):
        try:
            consent = models.Consent.objects.get(application_id=self.app_id, token=self.token)
            if not consent.application.app_id == self.app_id:
                raise exceptions.NotAcceptable("Invalid token or app id.")
        except models.Consent.DoesNotExist as e:
            raise exceptions.NotFound("Token does not exist") from e
        except models.Consent.MultipleObjectsReturned as e:
            raise exceptions.NotFound("Multiple tokens found") from e

def is_valid_app_id(app_id):
    qs = models.Application.objects.filter(app_id=app_id)
    if not qs.exists():
        raise exceptions.NotFound("Invalid application id")
