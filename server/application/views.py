from rest_framework import decorators, permissions
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_simplejwt.tokens import RefreshToken
from application import services, serializers, selectors, validators, models
from user.serializers import UserSerializer


class ApplicationViewSet(ModelViewSet):
    queryset = models.Application.objects.all()
    serializer_class = serializers.ApplicationSerializer


@decorators.api_view(["GET"])
def create_user_consent(request):
    user_id = request.user.id
    app_id = request.query_params.get("id")
    validators.is_valid_app_id(app_id)
    consent = services.create_user_consent(user_id, app_id)
    serializer = serializers.ConsentSerializer(consent)
    return Response(serializer.data)


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.AllowAny])
def open_id(request):
    app_id = request.query_params.get("id")
    app_secret = request.query_params.get("secret")
    token = request.query_params.get("token")
    validator = validators.ValidToken(app_id, app_secret, token)
    validator.is_secret_valid()
    validator.is_token_valid_for_app()
    user = selectors.get_user_from_token(token)
    serializer = UserSerializer(user, context={"request": request})
    return Response(serializer.data)


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.AllowAny])
def grant_token(request):
    app_id = request.query_params.get("id")
    app_secret = request.query_params.get("secret")
    token = request.query_params.get("token")
    validator = validators.ValidToken(app_id, app_secret, token)
    validator.is_secret_valid()
    validator.is_token_valid_for_app()
    user = selectors.get_user_from_token(token)
    refresh = RefreshToken.for_user(user)
    data = {"refresh": str(refresh), "access": str(refresh.access_token)}
    return Response(data)
