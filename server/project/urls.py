from django.urls import path, include
from rest_framework.routers import DefaultRouter
from application import views as application
from user import views as user
from django.contrib import admin

router = DefaultRouter()
router.register("user", user.UserViewSet)
router.register("o/application", application.ApplicationViewSet)

urlpatterns = [
    path("api/v1/admin/", admin.site.urls),
    path("api/v1/o/consent/", application.create_user_consent),
    path("api/v1/o/open-id/", application.open_id),
    path("api/v1/o/token/", application.grant_token),
    path("api/v1/token/", user.ObtainTokenView.as_view()),
    path("api/v1/", include(router.urls)),
]
