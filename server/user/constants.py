from django.db.models import TextChoices


class Roles(TextChoices):
    ADMIN = "admin", "admin"
    CUSTOMER = "customer", "customer"
    ASTROLOGER = "astrologer", "astrologer"
    SUPERUSER = "superuser", "superuser"
    QA = "qa", "qa"


class InternalRoles(TextChoices):
    ASTROLOGER = "astrologer", "astrologer"
    QA = "qa", "qa"


class Gender(TextChoices):
    MALE = "male", "male"
    FEMALE = "female", "female"
    OTHER = "other", "other"
