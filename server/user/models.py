from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from user.constants import Roles, Gender


class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("role", "superuser")
        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")
        return self.create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True, null=True)
    phone = models.CharField(max_length=25, blank=True, null=True)
    role = models.CharField(max_length=10, choices=Roles.choices, default=Roles.CUSTOMER)
    created_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(default="profile/default.jpg")
    gender = models.CharField(max_length=6, choices=Gender.choices, default=Gender.OTHER)
    scope = models.JSONField(default=dict)
    objects = UserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
