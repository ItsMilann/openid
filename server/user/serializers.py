from rest_framework import serializers
from user import models


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=255, write_only=True, required=True)

    class Meta:
        model = models.User
        fields = [
            "id",
            "first_name",
            "last_name",
            "email",
            "phone",
            "password",
            "role",
            "gender",
            "image",
            "scope"
        ]

    def create(self, validated_data):
        user = models.User.objects.create(**validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user

    def update(self, instance, validated_data):
        data = super().update(instance, validated_data)
        if validated_data.get("password"):
            instance.set_password("password")
            instance.save()
        return data