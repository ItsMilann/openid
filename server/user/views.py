from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from user import models, serializers


class ObtainTokenView(TokenObtainPairView):
    serializer_class = TokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        valid_data = serializer.validated_data
        return Response(valid_data, status=201)


class UserViewSet(ModelViewSet):
    queryset = models.User.objects.all().order_by("-id")
    serializer_class = serializers.UserSerializer
    permission_classes = [AllowAny]
    lookup_field = "id"

    def get_permissions(self):
        if self.request.method == "POST":
            self.permission_classes = [AllowAny]
        return super().get_permissions()

    @action(detail=True, methods=["post"])
    def set_password(self, request):
        user = self.get_object()
        password = request.data.get("password")
        old_password = request.data.get("old_password")
        valid_opw = authenticate(username=user.username, password=old_password)
        if password and old_password and valid_opw:
            user.set_password(password)
            user.save()
            return Response({"status": "password set"}, status=status.HTTP_200_OK)
        return Response({"message": "Invalid old password"}, status=status.HTTP_400_BAD_REQUEST)

    @action(["GET"], detail=False)
    def info(self, request):
        self.get_object = lambda: request.user
        return super().retrieve(request)
